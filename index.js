// console.log('Hello World');

// fetch() https://jsonplaceholder.typicode.com/posts

const fetchPosts = () => {
  fetch(`https://jsonplaceholder.typicode.com/posts`).then((response) => {
    response.json().then((data) => {
      console.log(data);
      return showPosts(data);
    });
  });
};
fetchPosts();

const postsContainer = document.querySelector('#div-post-entries');
let postEntries = '';

const showPosts = (posts) => {
  posts.forEach((post) => {
    postEntries += `
    <div id="post-id-${post.id}">
        <h3 id="post-title-${post.id}">${post.title}</h3>
        <p id="post-body-${post.id}">${post.body}</p>
        <button onClick= "editPost(${post.id})">Edit</button>
        <button onClick= "deletePost(${post.id})">Delete</button>
        <br><br>
    </div>
    `;
  });
  postsContainer.innerHTML = postEntries;
};

// add post
const addForm = document.querySelector('#form-add-post');

addForm.addEventListener('submit', (event) => {
  event.preventDefault();

  const titleInput = document.querySelector('#txt-title').value;
  const bodyInput = document.querySelector('#txt-body').value;

  fetch('https://jsonplaceholder.typicode.com/posts', {
    method: 'POST',
    headers: {
      'Content-type': 'application/json',
    },
    body: JSON.stringify({
      title: titleInput,
      body: bodyInput,
      userId: 1,
    }),
  })
    .then((response) => response.json())
    .then((data) => console.log(data));
  alert(`Succefully Added!`);
  document.querySelector('#txt-title').value = null;
  document.querySelector('#txt-body').value = null;
});

// Edit post

const editPost = (id) => {
  let title = document.querySelector(`#post-title-${id}`).innerHTML;
  let body = document.querySelector(`#post-body-${id}`).innerHTML;

  document.querySelector('#txt-edit-title').value = title;
  document.querySelector('#txt-edit-body').value = body;
  document.querySelector('#txt-edit-id').value = id;
  document.querySelector('#btn-submit-update').removeAttribute('disabled');
};

const editForm = document.querySelector('#form-edit-post');
editForm.addEventListener('submit', (event) => {
  event.preventDefault();
  const id = document.querySelector('#txt-edit-id').value;
  const editTitle = document.querySelector('#txt-edit-title').value;
  const editBody = document.querySelector('#txt-edit-body').value;

  fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
    method: 'PATCH',
    headers: {
      'Content-type': 'application/json',
    },
    body: JSON.stringify({
      title: editTitle,
      body: editBody,
    }),
  })
    .then((response) => response.json())
    .then((data) => console.log(data));
  alert(`Updated post!`);
  document.querySelector('#txt-edit-title').value = null;
  document.querySelector('#txt-edit-body').value = null;
  document.querySelector('#btn-submit-update').setAttribute('disabled', true);
});

// delete post
const deletePost = async (id) => {
  await fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
    method: 'DELETE',
  })
    .then((response) => response.json())
    .then((data) => alert(`Deleted post`));

  document.querySelector(`#post-id-${id}`).remove();
};
// const specificPost = (id) => {
//   if (id <= 100 && id >= 1) {
//     fetch(`https://jsonplaceholder.typicode.com/posts/${id}`)
//       .then((response) => response.json())
//       .then((data) => console.log(data));
//   }
// };
// specificPost(5);
